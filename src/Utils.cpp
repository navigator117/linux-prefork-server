#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdio>
#include <cerrno>
#include <cstring>
#include <cassert>
#include <cstdlib>
#include "Logger.hpp"
#include "Utils.hpp"

namespace prefork
{
    static int
    set_proc_argc = -1;
    static char**
    set_proc_argv = NULL;

    int
    run_as_daemon()
    {
        pid_t pid = fork();
        switch (pid) {
        case -1:
            fprintf(stderr, "run as daemon failed: %d, %s!\n", errno, strerror(errno));
            return -1;
        case 0:
            break;
        default:
            return pid;
        }
        if (setsid() == -1) {
            PREFORK_ERR("setsid() failed: %d, %s", errno, strerror(errno));
            return -1;
        }
        umask(0);
        int fd = open("/dev/null", O_RDWR);
        if (fd == -1) {
            PREFORK_ERR("open() \"/dev/null\" failed: %d, %s", errno, strerror(errno));
            return -1;
        }
        if (dup2(fd, STDIN_FILENO) == -1) {
            PREFORK_ERR("dup2() STDIN failed: %d, %s", errno, strerror(errno));
            return -1;                
        }
        if (dup2(fd, STDOUT_FILENO) == -1) {
            PREFORK_ERR("dup2() STDOUT failed: %d, %s", errno, strerror(errno));
            return -1;                
        }
        if (dup2(fd, STDERR_FILENO) == -1) {
            PREFORK_ERR("dup2() STDERR failed: %d, %s", errno, strerror(errno));
            return -1;
        }
        if (fd > STDERR_FILENO) {
            if (close(fd) == -1) {
                PREFORK_ERR("close() failed: %d, %s", errno, strerror(errno));
                return -1;
            }
        }
        return 0;            
    }

    void
    init_set_proc_title(int argc, char** argv)
    {
        set_proc_argc = argc;
        set_proc_argv = argv;
    }
        
    bool
    set_proc_title(const char* base, const char* title)
    {
        int argc    = set_proc_argc;
        char** argv = set_proc_argv;            
        assert(base != NULL && title != NULL && argc > 0 && argv != NULL);
        size_t available_mem_usage =
            argv[argc-1] - argv[0] + strlen(argv[argc-1]) + 1;
        size_t title_mem_usage =
            strlen(base) + strlen(":") +
            strlen(title) + 1;
        if (title_mem_usage > available_mem_usage) {
            int i;
            size_t env_mem_usage = 0;
            for (i=0; environ[i] != NULL; ++i) {
                env_mem_usage += strlen(environ[i]) + 1;
            }
            if (env_mem_usage == 0) {
                PREFORK_ERR("env_mem_usage == 0");
                return false;
            }
            available_mem_usage =
                environ[i-1] - argv[0] + strlen(environ[i-1]) + 1;
            if (title_mem_usage > available_mem_usage) {
                PREFORK_ERR("title_mem_usage > available_mem_usage");
                return false;
            }                
            char* p = NULL;
            if ((p=(char*)malloc(env_mem_usage)) == NULL) {
                PREFORK_ERR("(p=malloc(env_mem_usage)) == NULL");
                return false;
            }
            memcpy(p, environ[0], env_mem_usage);
            for (i=0; environ[i] != NULL; ++i) {
                environ[i] = p;
                p += strlen(environ[i]) + 1;
            }
        }
        memset(argv[0], 0, available_mem_usage);
        strcpy(argv[0], base);
        strcat(argv[0], ":");
        strcat(argv[0], title);
        set_proc_argc = 1;
        argv[1] = NULL;
        return true;
    }
}; // namespace prefork

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
