#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <cassert>
#include <cerrno>
#include <cstring>
#include <new>
#include "Logger.hpp"
#include "ShareMemory.hpp"
#include "ProcessPool.hpp"

namespace prefork
{
    ProcessPool&
    ProcessPool::get()
    {
        static ProcessPool pool;
        return pool;
    }

    void
    ProcessPool::sighandler(int signo)
    {
        assert(signo == PROCESS_SIGNAL_CHLD);
        PREFORK_DEBUG("pid %d got SIGCHLD", getpid());
        bool wait_ok = false;
        int status;
        pid_t child_pid = -1;
        while (1) {
            child_pid = waitpid(-1, &status, WNOHANG);
            if (child_pid == 0) {
                break;
            }
            else if (child_pid == -1) {
                if (errno == EINTR) {
                    continue;
                }
                else if (errno == ECHILD && wait_ok) {
                    break;
                }
                else {
                    PREFORK_ERR("waitpid failed %d, %s!", errno, strerror(errno));
                    break;
                }
            }
            else {
                wait_ok = true;
                if (WIFSIGNALED(status)) {
                    PREFORK_WARN("child %d kill by signo %d", child_pid, WTERMSIG(status));
                }
                else if (WIFEXITED(status)) {
                    PREFORK_INFO("child %d exited normal with code %d", child_pid, WEXITSTATUS(status));
                }
                ProcessPool::get().on_process_exit(child_pid);
            }                
        }
        Process::sighandler(signo);
    }
        
    ProcessPool::ProcessPool()
    {
        this->_shared = ShareMemory::create<SHARED_DATA>();
    }

    ProcessPool::~ProcessPool()
    {
        this->_shared = ShareMemory::remove<SHARED_DATA>(this->_shared);
    }

    const Process*
    ProcessPool::get_process(int index)
    {
        const Process* process = NULL;
        if (index >= 0 && index < MAX_NUM_OF_PROCESS) {
            process = &this->_processes[index];
        }
        else {                
            assert(false);
        }
        return process;
    }
        
    const ProcessGroup*
    ProcessPool::get_process_group(int index)
    {
        const ProcessGroup* process_group = NULL;
        if (index >= 0 && index < MAX_NUM_OF_PROCESS_GROUP) {
            process_group = &this->_process_groups[index];
        }
        else {                
            assert(false);
        }
        return process_group;            
    }
        
    const Process*
    ProcessPool::spawn(ProcessProc proc,
                       void* data/*= NULL*/,
                       ProcessGroup* process_group /*= NULL*/)
    {
        Process* process = NULL;
        for (int i=0; i<MAX_NUM_OF_PROCESS; ++i) {
            const Process* current = this->get_process(i);
            if (current->get_status() == PROCESS_STATUS_UNKNOWN ||
                current->get_status() == PROCESS_STATUS_STOPPED) {
                process = new (const_cast<Process*>(current))
                    Process(proc, data, process_group);
                process->set_index(i);
                process->set_shared(this->_shared);             
            }
        }
        if (process != NULL && process->fork() > 0) {
            return process;
        }
        else {
            PREFORK_ERR("process pool exhausted!");
            return NULL;
        }
    }

    const ProcessGroup*
    ProcessPool::spawn_many(int num_of_process,
                            ProcessProc proc, 
                            void* data /*= NULL*/)
    {
        assert(num_of_process <= MAX_NUM_OF_PROCESS_IN_GROUP);
        ProcessGroup* process_group = NULL;
        for (int i=0; i<MAX_NUM_OF_PROCESS_GROUP; ++i) {
            const ProcessGroup* current = this->get_process_group(i);
            if (current->get_status() == PROCESS_GROUP_STATUS_UNKNOWN ||
                current->get_status() == PROCESS_GROUP_STATUS_STOPPED) {
                process_group = new (const_cast<ProcessGroup*>(current))
                    ProcessGroup(num_of_process);
                break;
            }
        }
        if (process_group != NULL) {
            process_group->set_status(PROCESS_GROUP_STATUS_STARTED);
            int index = 0;
            while (index < num_of_process) {
                const Process* process =
                    this->spawn(proc, data, process_group);
                if (process != NULL) {
                    process_group->set_process(index, process);
                    ++index;
                }
                else {
                    assert(false);
                }
            }
        }
        else {
            assert(false);
            PREFORK_ERR("processgroup pool exhausted!");
        }
        return process_group;
    }
        
    void
    ProcessPool::on_process_exit(pid_t child_pid)
    {
        Process* process = NULL;
        ProcessGroup* process_group = NULL;
        for (int i=0; i<MAX_NUM_OF_PROCESS; ++i) {
            process = const_cast<Process*>(this->get_process(i));
            if (process != NULL && process->get_pid() == child_pid) {
                if (process->get_status() == PROCESS_STATUS_STOPPING) {
                    process->set_status(PROCESS_STATUS_STOPPED);
                    process_group = process->get_group();
                }
                else if (process->get_status() == PROCESS_STATUS_STARTED) {
                    PREFORK_INFO("process %d need respawn!", process->get_pid());
                    process->set_status(PROCESS_STATUS_STARTING);
                }
                else {
                    assert(false);
                    process->set_status(PROCESS_STATUS_STOPPED);
                }
                break;
            }
        }
        if (process_group != NULL) {
            int i=0;
            for (; i<process_group->get_process_count(); ++i) {
                if (process_group->get_process(i)->get_status() !=
                    PROCESS_STATUS_STOPPED) {
                    break;
                }
            }
            if (i == process_group->get_process_count()) {
                process_group->set_status(PROCESS_GROUP_STATUS_STOPPED);
            }
        }
    }

    void
    ProcessPool::respawn_processes()
    {
        Process* process = NULL;
        for (int i=0; i<MAX_NUM_OF_PROCESS; ++i) {
            process = const_cast<Process*>(this->get_process(i));
            if (process != NULL && process->get_status() == PROCESS_STATUS_STARTING) {
                PREFORK_INFO("process %d respawning!", process->get_pid());
                process->fork();
            }
        }
    }
}; // namespace prefork

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
