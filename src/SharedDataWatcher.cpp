#include "Logger.hpp"
#include "Process.hpp"
#include "ProcessPool.hpp"
#include "SharedDataWatcher.hpp"

namespace prefork
{
    SharedDataWatcher::SharedDataWatcher(pid_t parent,
                                         int index,
                                         SHARED_DATA* shared,
                                         const PREFORK_CONFIG& config) :
        _parent(parent),
        _index(index),
        _shared(shared),
        _config(config),
        _less_woker_counter(0),
        _more_woker_counter(0)            
    {
    }

    SharedDataWatcher::~SharedDataWatcher()
    {
    }

    void
    SharedDataWatcher::init()
    {
    }

    void
    SharedDataWatcher::fini()
    {
    }

    void
    SharedDataWatcher::query()
    {
        int spawned = this->_shared->_spawned - 1;
        int counter = 0;
        for (int i=0; i<MAX_NUM_OF_PROCESS; ++i) {
            if (i == this->_index) {
                continue;
            }
            if (this->_shared->_is_busy[i]) {
                ++counter;
            }
        }
        float ratio = static_cast<float>(counter) / spawned;
        if (ratio > this->_config._inc_worker_ratio) {
            if (++this->_more_woker_counter >=
                this->_config._inc_worker_hits) {
                if (kill(this->_parent,
                         PROCESS_SIGNAL_MORE_WORKER) < 0) {
                    PREFORK_ERR("kill process %d with signal %s failed",
                                this->_parent,
                                SignalHelper::get_signal_name(PROCESS_SIGNAL_MORE_WORKER));
                }
                PREFORK_DEBUG("need more worker!");
                this->_more_woker_counter = 0;
            }
            this->_less_woker_counter = 0;
        }
        else if (ratio < this->_config._dec_worker_ratio) {
            if (spawned > this->_config._num_of_workers) {
                if (++this->_less_woker_counter >=
                    this->_config._dec_worker_hits) {
                    if (kill(this->_parent,
                             PROCESS_SIGNAL_LESS_WORKER) < 0) {
                        PREFORK_ERR("kill process %d with signal %s failed",
                                    this->_parent,
                                    SignalHelper::get_signal_name(PROCESS_SIGNAL_LESS_WORKER));
                    }
                    PREFORK_DEBUG("need less worker!");
                    this->_less_woker_counter = 0;
                }
                this->_more_woker_counter = 0;
            }
        }
        else {
            this->_less_woker_counter = 0;
            this->_more_woker_counter = 0;
        }
        PREFORK_INFO("busy: %d, "
                     "spawned: %d, "
                     "ratio: %f, "
                     "less worker counter %d, "
                     "dec worker hits %d, "
                     "more worker counter %d, "
                     "inc worker hits %d ",
                     counter,
                     spawned,
                     ratio,
                     this->_less_woker_counter,
                     this->_config._dec_worker_hits,
                     this->_more_woker_counter,
                     this->_config._inc_worker_hits);
    }
}; // namespace prefork

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
