#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <cassert>
#include <cstdlib>
#include <cerrno>
#include <cstring>
#include "Logger.hpp"
#include "SignalHelper.hpp"
#include "Process.hpp"
#include "ProcessPool.hpp"

namespace prefork
{
    volatile int
    Process::_events = PROCESS_EVENT_NONE;
        
    void
    Process::sighandler(int signo)
    {
        PREFORK_DEBUG("pid %d got signal %s",
                  getpid(), SignalHelper::get_signal_name(signo));
        switch (signo) {
        case PROCESS_SIGNAL_CHLD:
            Process::_events |= PROCESS_EVENT_CHLD;
            break;
        case PROCESS_SIGNAL_TERM:
            Process::_events |= PROCESS_EVENT_TERM;
            break;
        case PROCESS_SIGNAL_EXIT:
            Process::_events |= PROCESS_EVENT_EXIT;
            break;
        case PROCESS_SIGNAL_QUIT:
            Process::_events |= PROCESS_EVENT_QUIT;
            break;
        case PROCESS_SIGNAL_MORE_WORKER:
            Process::_events |= PROCESS_EVENT_MORE_WORKER;
            break;
        case PROCESS_SIGNAL_LESS_WORKER:
            Process::_events |= PROCESS_EVENT_LESS_WORKER;
            break;
        default:
            break;
        }
    }

    Process::Process() :
        _index(-1),
        _shared(NULL),
        _pid(-1),
        _status(PROCESS_STATUS_UNKNOWN),
        _proc(NULL),
        _data(NULL),
        _group(NULL)            
    {
    }
        
    Process::Process(ProcessProc proc,
                     void* data /*= NULL*/,
                     ProcessGroup* group /*= NULL*/):
        _index(-1),
        _shared(NULL),
        _pid(-1),
        _status(PROCESS_STATUS_STOPPED),
        _proc(proc),
        _data(data),
        _group(group)
    {
    }

    Process::~Process()
    {
    }

    void
    Process::main()
    {
        this->set_pid(getpid());
        this->set_status(PROCESS_STATUS_STARTED);
        if (this->_proc != NULL) {
            this->_proc(*this, this->_data);             
        }
        exit(0);
    }
        
    pid_t
    Process::fork()
    {
        pid_t old_pid = this->get_pid();
        pid_t new_pid = -1;
        if ((new_pid=::fork()) == 0) {
            this->main();
        }
        else if (new_pid > 0) {
            this->set_pid(new_pid);
            this->set_status(PROCESS_STATUS_STARTED);
            if (old_pid != -1) {
                PREFORK_INFO("process %d respawn to process %d",
                         old_pid, new_pid);
            }
        }
        else {
            PREFORK_ERR("spawn process failed!");
        }
        return new_pid;
    }

    int
    Process::get_index() const
    {
        assert(this->_index != -1);
        return this->_index;
    }

    void
    Process::set_index(int index)
    {
        assert(this->_index == -1 && index != -1);
        this->_index = index;
    }

    SHARED_DATA*
    Process::get_shared() const
    {
        return this->_shared;
    }
    void
    Process::set_shared(SHARED_DATA* shared)
    {
        assert(this->_shared == NULL && shared != NULL);
        this->_shared = shared;
    }
        
    pid_t
    Process::get_pid() const
    {
        return this->_pid;
    }
        
    void
    Process::set_pid(pid_t pid)
    {
        this->_pid = pid;
    }

    bool
    Process::in_child_process() const
    {
        return (this->get_pid() == getpid());
    }
        
    int
    Process::get_status() const
    {
        return this->_status;
    }
        
    void
    Process::set_status(int status)
    {
        this->_status = status;
        if (!this->in_child_process() && this->_shared != NULL) {
            if (this->_status == PROCESS_STATUS_STARTED) {
                ++this->_shared->_spawned;
                PREFORK_INFO("process %d started!", this->get_pid());
            }
            if (this->_status == PROCESS_STATUS_STOPPED) {
                --this->_shared->_spawned;
                PREFORK_INFO("process %d stopped!", this->get_pid());
            }
        }
    }

    int
    Process::get_events() const
    {
        return Process::_events;
    }

    const char*
    Process::get_events_desc() const
    {
        static char result[] =
            "TERM|EXIT|QUIT|CHLD|MORE_WORKER|LESS_WORKER";
        memset(result, 0, sizeof(result));
        if (Process::_events != PROCESS_EVENT_NONE) {
            struct {
                const int   type;
                const char* name;
            } descs[] = {
                {PROCESS_EVENT_TERM, "TERM" },
                {PROCESS_EVENT_EXIT, "EXIT" },            
                {PROCESS_EVENT_QUIT, "QUIT" },
                {PROCESS_EVENT_CHLD, "CHLD" },
                {PROCESS_EVENT_MORE_WORKER, "MORE_WORKER" },
                {PROCESS_EVENT_LESS_WORKER, "LESS_WORKER" }                
            };            
            bool need_or = false;
            for (size_t i=0; i<sizeof(descs)/sizeof(descs[0]); ++i) {
                if (Process::_events & descs[i].type) {
                    if (need_or) {
                        strcat(result, "|");
                    }
                    strcat(result, descs[i].name);
                    need_or = true;
                }
            }
        }
        else {
            strcpy(result, "NONE");
        }
        return result;
    }
        
    void
    Process::rst_events() const
    {
        Process::_events = PROCESS_EVENT_NONE;
    }
        
    void
    Process::quit(bool wait) const
    {
        assert(!this->in_child_process());
        if (this->get_status() == PROCESS_STATUS_STARTED) {
            const_cast<Process*>(this)->set_status(PROCESS_STATUS_STOPPING);
            if (kill(this->get_pid(), SIGQUIT) < 0) {
                assert(errno == ESRCH);
                PREFORK_ERR("kill process %d with signal %s failed",
                        this->get_pid(),
                        SignalHelper::get_signal_name(PROCESS_SIGNAL_QUIT));
            }
            if (wait) {
                while (this->get_status() != PROCESS_STATUS_STOPPED) {
                    usleep(100 * 1000);
                }
            }
        }
        else if (this->get_status() == PROCESS_STATUS_STARTING) {
            PREFORK_INFO("process %d is waiting for respawn, "
                     "set it to stopped status", this->get_pid());
            const_cast<Process*>(this)->set_status(PROCESS_STATUS_STOPPED);
        }
    }

    ProcessGroup*
    Process::get_group() const
    {
        return this->_group;
    }
}; // namespace prefork

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
