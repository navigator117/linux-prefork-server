#include <cstdio>
#include <cmath>
#include <cstring>
#include "PreforkConfig.h"

void
check_prefork_config(PREFORK_CONFIG* config)
{
    if (config->_process_base_name[0] == 0) {
        strcpy(config->_process_base_name, DEFAULT_PROCESS_BASE_NAME);
    }    
    if (config->_port == 0) {
        config->_port = DEFAULT_SERVER_PORT;
    }
    if (config->_backlog == 0) {
        config->_backlog = DEFAULT_BACKLOG_SIZE;
    }
    if (fabs(config->_workload_check_interval) < 0.00001) {
        config->_workload_check_interval = DEFAULT_WORKLOAD_CHECK_INTERVAL;
    }
    if (fabs(config->_inc_worker_ratio) < 0.00001) {
        config->_inc_worker_ratio = DEFAULT_INC_WORKER_RATIO;
    }
    if (fabs(config->_dec_worker_ratio) < 0.00001) {
        config->_dec_worker_ratio = DEFAULT_DEC_WORKER_RATIO;
    }
    if (config->_inc_worker_hits == 0) {
        config->_inc_worker_hits = DEFAULT_INC_WORKER_HITS;
    }
    if (config->_dec_worker_hits == 0) {
        config->_dec_worker_hits = DEFAULT_DEC_WORKER_HITS;
    }
    if (config->_num_of_workers == 0) {
        config->_num_of_workers = DEFAULT_NUM_OF_WORKER_PROCESS;
    }
    if (config->_inc_of_workers == 0) {
        config->_inc_of_workers = DEFAULT_INC_OF_WORKER_PROCESS;
    }
    fprintf(stderr,
            "PreforkServer config with: \n"
            "process base name: %s\n"
            "run as daemon: %d\n"
            "run in single process mode: %d\n"
            "port: %d\n"
            "backlog: %d\n"
            "workload check interval: %.3f secs\n"
            "inc worker ratio: %.1f\n"
            "dec worker ratio: %.1f\n"
            "inc worker hits: %d\n"
            "dec worker hits: %d\n"
            "num of workers: %d\n"
            "inc of workers: %d\n",
            config->_process_base_name,
            config->_is_daemon,
            config->_is_single_process,
            config->_port,
            config->_backlog,
            config->_workload_check_interval,
            config->_inc_worker_ratio,
            config->_dec_worker_ratio,
            config->_inc_worker_hits,
            config->_dec_worker_hits,
            config->_num_of_workers,
            config->_inc_of_workers);
}

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
