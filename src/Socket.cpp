#include <cerrno>
#include <cstring>
#include "Logger.hpp"
#include "Socket.hpp"

namespace prefork
{
    int
    Socket::open() {
        if (this->_sock == -1) {
            if ((this->_sock=socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                PREFORK_ERR("errno: %d, %s", errno, strerror(errno));
            }
        }
        return this->_sock;
    }

    ssize_t
    Socket::block_send(const unsigned char* const buffer, size_t want_bytes) {
        size_t bytes_not_write;
        ssize_t bytes_written;    
        const unsigned char* p = buffer;
        bytes_not_write = want_bytes;
        while (bytes_not_write > 0) {
            if ((bytes_written=write(this->sock(), p, bytes_not_write)) <= 0) {
                if (bytes_written < 0 && errno == EINTR) {
                    bytes_written = 0;
                }
                else {
                    PREFORK_ERR("errno: %d, %s", errno, strerror(errno));
                    return -1;
                }
            }
            bytes_not_write -= bytes_written;
            p += bytes_written;
        }
        return want_bytes;
    }

    ssize_t
    Socket::block_recv(unsigned char* buffer, size_t want_bytes) {
        size_t bytes_not_read;
        ssize_t bytes_readed;
        unsigned char* p = buffer;
        bytes_not_read = want_bytes;
        while (bytes_not_read > 0) {
            if ((bytes_readed=read(this->sock(), p, bytes_not_read)) < 0) {
                if (errno == EINTR) {
                    bytes_readed = 0;
                }
                else {
                    PREFORK_ERR("errno: %d, %s", errno, strerror(errno));
                    return -1;
                }
            }
            else if (bytes_readed == 0) {
                break;
            }
            bytes_not_read -= bytes_readed;
            p += bytes_readed;
        }
        return (want_bytes - bytes_not_read);
    }
}; // namespace prefork

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
