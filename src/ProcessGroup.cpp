#include <cstring>
#include <cassert>
#include "Logger.hpp"
#include "ProcessGroup.hpp"

namespace prefork
{
    ProcessGroup::ProcessGroup():
        _num_of_process(0),
        _status(PROCESS_GROUP_STATUS_UNKNOWN)
    {
        memset(this->_processes, 0, sizeof(this->_processes));
    }

    ProcessGroup::ProcessGroup(int num_of_process):
        _num_of_process(num_of_process),
        _status(PROCESS_GROUP_STATUS_STOPPED)
    {
        memset(this->_processes, 0, sizeof(this->_processes));
    }

    ProcessGroup::~ProcessGroup()
    {
    }

    int
    ProcessGroup::get_status() const
    {
        return this->_status;
    }

    void
    ProcessGroup::set_status(int status)
    {
        this->_status = status;
    }

    void
    ProcessGroup::set_process(int index, const Process* process)
    {
        assert(index >= 0 && index < this->get_process_count());
        this->_processes[index] = process;
    }

    const Process*
    ProcessGroup::get_process(int index) const
    {
        assert(index >= 0 && index < this->get_process_count());
        return this->_processes[index];
    }
        
    int
    ProcessGroup::get_process_count() const
    {
        return this->_num_of_process;
    }

    void
    ProcessGroup::quit(bool wait) const
    {
        for (int i=0; i<this->get_process_count(); ++i) {
            this->get_process(i)->quit(false);
        }
        if (wait) {
            while (this->get_status() != PROCESS_GROUP_STATUS_STOPPED) {
                usleep(100 * 1000);
            }
        }
    }
}; // namespace prefork

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
