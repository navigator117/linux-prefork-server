#include <asm/types.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/netlink.h>
#include <linux/inet_diag.h>
#include <netinet/tcp.h>
#include <cstring>
#include "Logger.hpp"
#include "Process.hpp"
#include "BacklogWatcher.hpp"

namespace prefork
{
    BacklogWatcher::BacklogWatcher(pid_t parent,
                                   const PREFORK_CONFIG& config) :
        _parent(parent),
        _sock(-1),
        _config(config),
        _less_woker_counter(0),
        _more_woker_counter(0)
    {
    }
        
    BacklogWatcher::~BacklogWatcher()
    {
    }                

    void
    BacklogWatcher::init()
    {
        struct sockaddr_nl nl_src_addr;
        memset(&nl_src_addr, 0, sizeof(nl_src_addr));
            
        nl_src_addr.nl_family = AF_NETLINK;
        nl_src_addr.nl_pid    = getpid();
        nl_src_addr.nl_groups = 0;

        if ((this->_sock=socket(AF_NETLINK,
                                SOCK_RAW,
                                NETLINK_INET_DIAG)) < 0) {
            PREFORK_ERR("create NETLINK sock failed!\n");
            return;
        }
        if (bind(this->_sock,
                 (struct sockaddr*)&nl_src_addr,
                 sizeof(nl_src_addr)) < 0) {
            PREFORK_ERR("bind NETLINK sock failed!\n");        
            return;
        }
    }

    void
    BacklogWatcher::fini()
    {
        if (this->_sock) {
            close(this->_sock);
            this->_sock = NULL;
        }
    }
        
    void
    BacklogWatcher::query()
    {
        struct {
            struct nlmsghdr      header;
            struct inet_diag_req body;
        } nl_request;
        struct sockaddr_nl nl_dst_addr;
        memset(&nl_request, 0, sizeof(nl_request));
        memset(&nl_dst_addr, 0, sizeof(nl_dst_addr));    
        nl_dst_addr.nl_family              = AF_NETLINK;
        nl_dst_addr.nl_pid                 = 0;
        nl_dst_addr.nl_groups              = 0;
        nl_request.header.nlmsg_len        = sizeof(nl_request);
        nl_request.header.nlmsg_type       = TCPDIAG_GETSOCK;
        nl_request.header.nlmsg_flags      = NLM_F_REQUEST;
        nl_request.header.nlmsg_pid        = getpid();
        nl_request.body.idiag_family       = AF_INET;
        nl_request.body.idiag_states       = (1 << TCP_LISTEN);
        nl_request.body.id.idiag_sport     = htons(this->_config._port);
        nl_request.body.id.idiag_cookie[0] = INET_DIAG_NOCOOKIE;
        nl_request.body.id.idiag_cookie[1] = INET_DIAG_NOCOOKIE;
        struct iovec iov_send = {
            &nl_request,
            nl_request.header.nlmsg_len
        };            
        struct msghdr send_msg = {
            (void*)&nl_dst_addr,
            sizeof(nl_dst_addr),
            &iov_send,
            1, NULL, 0, 0                
        };            
        if (sendmsg(this->_sock, &send_msg, 0) < 0) {
            PREFORK_ERR("sendto NETLINK sock failed!\n");
            return;
        }
        struct sockaddr_nl nl_src_addr;
        memset(&nl_src_addr, 0, sizeof(nl_src_addr));
        char buffer[4096];
        memset(buffer, 0, sizeof(buffer));
        struct iovec iov_recv = {
            buffer,
            sizeof(buffer)
        };
        struct msghdr recv_msg = {
            (void*)&nl_src_addr,
            sizeof(nl_src_addr),
            &iov_recv,
            1, NULL, 0, 0
        };
        int recv_len = 0;
        if ((recv_len=
             recvmsg(this->_sock, &recv_msg, 0)) <= 0) {
            return;
        }
        for (struct nlmsghdr* recv_header=(struct nlmsghdr*)buffer;
             NLMSG_OK(recv_header, recv_len);
             recv_header=NLMSG_NEXT(recv_header, recv_len)) {
            if (recv_header->nlmsg_type == NLMSG_DONE) {
                break;
            }
            else if (recv_header->nlmsg_type == NLMSG_ERROR) {
                struct nlmsgerr* recv_error =
                    (struct nlmsgerr*)NLMSG_DATA(recv_header);
                PREFORK_ERR("%s, %d\n",
                        strerror(-recv_error->error),
                        -recv_error->error);
                break;
            }
            else {
                struct inet_diag_msg* inet_diag_msg =
                    (struct inet_diag_msg*)
                    NLMSG_DATA(recv_header);
                if (inet_diag_msg->idiag_state == TCP_LISTEN) {
                    this->tell_parent(inet_diag_msg->idiag_rqueue,
                                      inet_diag_msg->idiag_wqueue);
                    break;
                }
            }
        }
    }

    void
    BacklogWatcher::tell_parent(unsigned int backlog,
                                unsigned int max_backlog) 
    {
        PREFORK_DEBUG("BACKLOG: %u, MAX_BACKLOG: %u", backlog, max_backlog);
        float ratio = static_cast<float>(backlog) / max_backlog;
        if (ratio > this->_config._inc_worker_ratio) {
            if (++this->_more_woker_counter >=
                this->_config._inc_worker_hits) {
                if (kill(this->_parent,
                         PROCESS_SIGNAL_MORE_WORKER) < 0) {
                    PREFORK_ERR("kill process %d with signal %s failed",
                            this->_parent,
                            SignalHelper::get_signal_name(PROCESS_SIGNAL_MORE_WORKER));
                }
                PREFORK_DEBUG("need more worker!");
                this->_more_woker_counter = 0;
            }
            this->_less_woker_counter = 0;
        }            
        else if (ratio < this->_config._dec_worker_ratio) {
            if (++this->_less_woker_counter >=
                this->_config._dec_worker_hits) {
                if (kill(this->_parent,
                         PROCESS_SIGNAL_LESS_WORKER) < 0) {
                    PREFORK_ERR("kill process %d with signal %s failed",
                            this->_parent,
                            SignalHelper::get_signal_name(PROCESS_SIGNAL_LESS_WORKER));
                }
                PREFORK_DEBUG("need less worker!");
                this->_less_woker_counter = 0;
            }
            this->_more_woker_counter = 0;                
        }
        else {
            this->_less_woker_counter = 0;
            this->_more_woker_counter = 0;
        }
        PREFORK_INFO("ratio: %f, "
                     "less worker counter %d, "
                     "dec worker hits %d, "
                     "more worker counter %d, "
                     "inc worker hits %d",
                     ratio,
                     this->_less_woker_counter,
                     this->_config._dec_worker_hits,
                     this->_more_woker_counter,
                     this->_config._inc_worker_hits);
    }
}; // namespace prefork

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
