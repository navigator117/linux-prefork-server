#include <signal.h>
#include <cstdlib>
#include <cstdio>
#include "Utils.hpp"
#include "Logger.hpp"
#include "PreforkServer.hpp"
#include "BacklogWatcher.hpp"
#include "SharedDataWatcher.hpp"

namespace prefork
{
    void
    PreforkServer::main_proc(const Process& process, PreforkServer* self)
    {            
        self->init_network();
        if (self->_job_init != NULL) {
            if (self->_job_init() < 0) {
                fprintf(stderr, "Init Job failed!\n");
                exit(-1);
            }
        }
        if (self->_config._is_daemon) {
            pid_t pid = run_as_daemon();
            switch (pid) {
            case -1:
                exit(-1);
            case 0:
                break;
            default:
                exit(0);
            }
        }
        if (!self->_config._is_single_process) {
            self->master_main_loop(process);
        }
        else {
            self->single_main_loop(process);
        }
        self->fini_network();
        PREFORK_INFO("PreforkServer is exited!\n");
    }

    void
    PreforkServer::watch_proc(const Process& process, PreforkServer* self)
    {
        set_proc_title(self->_config._process_base_name,
                       WATCHER_PROCESS_TITLE);
#ifdef BACKLOG_WATCHER
        BacklogWatcher watcher(self->get_pid(), self->_config);
#else  /* !BACKLOG_WATCHER */
        SharedDataWatcher watcher(self->get_pid(),
                                  process.get_index(),
                                  process.get_shared(),
                                  self->_config);
#endif /* !BACKLOG_WATCHER */            
        watcher.init();
        SignalHelper::ignore_signals();            
        SignalHelper::allow_signals();
        for (;;) {
            if (process.get_events() & PROCESS_EVENT_QUIT) {
                break;
            }
            useconds_t usecs =
                static_cast<useconds_t>(self->_config.
                                        _workload_check_interval *
                                        1000 * 1000);
            usleep(usecs);
            watcher.query();
        }
        watcher.fini();
    }

    void
    PreforkServer::work_proc(const Process& process, PreforkServer* self)
    {
        set_proc_title(self->_config._process_base_name,
                       WORKER_PROCESS_TITLE);
        Socket peer_sock;
        SignalHelper::ignore_signals();
        SignalHelper::allow_signals();
        for (;;) {                
            if (process.get_events() & PROCESS_EVENT_QUIT) {
                break;
            }
            if (!self->_sock.accept(peer_sock)) {
                continue;
            }                
            int index = process.get_index();
            SHARED_DATA* shared = process.get_shared();
            shared->_is_busy[index] = true;                
            if (self->_job_call != NULL) {
                self->_job_call(peer_sock.sock());
            }
            else {
                PREFORK_WARN("client accepted, but have nothing to do!");
            }
            shared->_is_busy[index] = false;
            peer_sock.close();
        }
    }
        
    PreforkServer::PreforkServer(const PREFORK_CONFIG& config,
                                 JobInitFunc job_init /*= NULL*/,
                                 JobCallback job_call /*= NULL*/):
        Process((ProcessProc)(PreforkServer::main_proc), this),
        _config(config),
        _job_init(job_init),
        _job_call(job_call),
        _watcher(NULL),
        _free_work_groups_index(0)
    {
        memset(this->_work_groups, 0, sizeof(this->_work_groups));
        check_prefork_config(&this->_config);
    }

    PreforkServer::~PreforkServer()
    {
    }

    void
    PreforkServer::init_watcher()
    {
        this->_watcher =
            ProcessPool::get().spawn((ProcessProc)(PreforkServer::watch_proc),
                                     this);
        assert(this->_watcher != NULL);
    }
        
    void
    PreforkServer::fini_watcher()
    {
        this->_watcher->quit(true);
        this->_watcher = NULL;
    }
        
    void
    PreforkServer::inc_work_groups(int num_of_process_in_group)
    {
        if (this->_free_work_groups_index < MAX_NUM_OF_PROCESS_GROUP) {
            const ProcessGroup* process_group =
                ProcessPool::get().spawn_many(num_of_process_in_group,
                                              (ProcessProc)(PreforkServer::work_proc),
                                              this);
            if (process_group != NULL) {
                this->_work_groups[this->_free_work_groups_index] = process_group;
                this->_free_work_groups_index++;
                PREFORK_INFO("inc %d workers!", num_of_process_in_group);
                return;
            }
        }
        assert(false);
        PREFORK_WARN("can't spawn more workers!");
    }
        
    void
    PreforkServer::dec_work_groups(bool wait)
    {
        if (this->_free_work_groups_index > 0) {
            const ProcessGroup* process_group =
                this->_work_groups[this->_free_work_groups_index - 1];
            this->_work_groups[this->_free_work_groups_index - 1] = NULL;
            if (process_group != NULL) {
                process_group->quit(wait);
                this->_free_work_groups_index--;
                PREFORK_INFO("dec %d workers!", process_group->get_process_count());
            }
        }
        else {
            assert(false);
        }
    }

    void
    PreforkServer::init_network()
    {
        if (this->_sock.open() < 0) {
            fprintf(stderr, "open socket failed!\n");
            exit(-1);
        }
        int set = 1;
        if (setsockopt(this->_sock.sock(),
                       SOL_SOCKET,
                       SO_REUSEADDR,
                       (const char*)&set,
                       sizeof(set)) != 0) {
            fprintf(stderr, "set SO_REUSEADDR failed!\n");
            exit(-1);
        }
        if (setsockopt(this->_sock.sock(),
                       SOL_SOCKET,
                       TCP_NODELAY,
                       (const char*)&set,
                       sizeof(set)) != 0) {
            fprintf(stderr, "set TCP_NODELAY failed!\n");
            exit(-1);
        }
        if (setsockopt(this->_sock.sock(),
                       SOL_SOCKET,
                       TCP_DEFER_ACCEPT,
                       (const char*)&set,
                       sizeof(set)) != 0) {
            fprintf(stderr, "set TCP_DEFER_ACCEPT failed!\n");
            exit(-1);
        }
        SockAddr addr(this->_config._port);
        if (this->_sock.bind(addr) < 0) {
            fprintf(stderr, "bind socket failed!\n");
            exit(-1);
        }
        if (this->_sock.listen(this->_config._backlog) < 0) {
            fprintf(stderr, "listen socket failed!\n");
            exit(-1);
        }
        fprintf(stderr, "listen connections at: %d\n", this->_config._port);
    }

    void
    PreforkServer::fini_network()
    {
        this->_sock.close();
    }

    void
    PreforkServer::single_main_loop(const Process& process)
    {
        SignalHelper::register_signals();
        Socket peer_sock;
        for (;;) {
            if ((process.get_events() & PROCESS_EVENT_EXIT) ||
                (process.get_events() & PROCESS_EVENT_TERM)) {
                break;
            }
            if (!this->_sock.accept(peer_sock)) {
                continue;
            }
            if (this->_job_call != NULL) {
                this->_job_call(peer_sock.sock());
            }
            else {
                PREFORK_WARN("client accepted, but have nothing to do!");
            }
            peer_sock.close();
        }
    }
        
    void
    PreforkServer::master_main_loop(const Process& process)
    {
        SignalHelper::register_signals();
        SignalHelper::block_signals();
        this->inc_work_groups(this->_config._num_of_workers);
        this->init_watcher();
        for (;;) {
            SignalHelper::pause_wait_signals();
            PREFORK_DEBUG("master got events %s!", process.get_events_desc());
            if ((process.get_events() & PROCESS_EVENT_EXIT) ||
                (process.get_events() & PROCESS_EVENT_TERM)) {
                break;
            }
            if (process.get_events() & PROCESS_EVENT_CHLD) {
                ProcessPool::get().respawn_processes();
            }
            if (process.get_events() & PROCESS_EVENT_MORE_WORKER) {
                this->inc_work_groups(this->_config._inc_of_workers);
            }
            if (process.get_events() & PROCESS_EVENT_LESS_WORKER) {
                if (this->_free_work_groups_index - 1 > DEFAULT_WORK_GROUP_INDEX) {
                    this->dec_work_groups(false);
                }
            }
            process.rst_events();
        }
        SignalHelper::allow_signals();
        PREFORK_INFO("PreforkServer is exiting, please wait a while...\n");
        this->fini_watcher();
        while (this->_free_work_groups_index > 0) {
            this->dec_work_groups(true);
        }        
    }
}; // namespace prefork

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
