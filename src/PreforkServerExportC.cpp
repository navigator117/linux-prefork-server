#include <getopt.h>
#include <cstdio>
#include "Utils.hpp"
#include "PreforkServer.hpp"
#include "PreforkServerExportC.h"

using namespace prefork;

void
prefork_usage(const char* program)
{
    fprintf(stderr, "Usage: %s [OPTIONS] ... \n"
            "\t -t process base name, default is %s\n"
            "\t -r run as daemon service\n"
            "\t -s run in single process mode\n"
            "\t -p port, default is %d\n"
            "\t -b backlog, default is %d\n"
            "\t -c workload check interval, default is %.3f secs\n"
            "\t -m inc worker ratio, default is %.1f\n"
            "\t -x dec worker ratio, default is %.1f\n"
            "\t -i inc worker hits, default is %d\n"
            "\t -d dec worker hits, default is %d\n"
            "\t -n num of workers, default is %d\n"
            "\t -g inc of workers, default is %d\n"
            "\t -h help\n",
            program,
            DEFAULT_PROCESS_BASE_NAME,
            DEFAULT_SERVER_PORT,
            DEFAULT_BACKLOG_SIZE,
            DEFAULT_WORKLOAD_CHECK_INTERVAL,
            DEFAULT_INC_WORKER_RATIO,
            DEFAULT_DEC_WORKER_RATIO,
            DEFAULT_INC_WORKER_HITS,
            DEFAULT_DEC_WORKER_HITS,
            DEFAULT_NUM_OF_WORKER_PROCESS,
            DEFAULT_INC_OF_WORKER_PROCESS);
}

int
prefork_parse(int argc,
              char* argv[],
              PREFORK_CONFIG* config)
{
    int optval;
	while ((optval=getopt(argc, argv, "t:rsp:b:c:m:x:i:d:n:g:h")) != -1) {
		switch (optval) {
        case 't':
            strcpy(config->_process_base_name, optarg);
            break;
        case 'r':
            config->_is_daemon = 1;
            break;
        case 's':
            config->_is_single_process = 1;
            break;
		case 'p':
            config->_port = atoi(optarg);
            break;
		case 'b':
            config->_backlog = atoi(optarg);
            break;
		case 'c':
            config->_workload_check_interval = static_cast<float>(atof(optarg));
            break;
		case 'm':
            config->_inc_worker_ratio = static_cast<float>(atof(optarg));
            break;
		case 'x':
            config->_dec_worker_ratio = static_cast<float>(atof(optarg));
            break;
		case 'i':
            config->_inc_worker_hits = atoi(optarg);
            break;          
		case 'd':
            config->_dec_worker_hits = atoi(optarg);
            break;
		case 'n':
            config->_num_of_workers = atoi(optarg);
            break;
		case 'g':
            config->_inc_of_workers = atoi(optarg);
            break;
		case 'h':
		default:
            return -1;
		}
    }
    return 0;
}
          
void
prefork_serve(int argc,
              char* argv[],
              const PREFORK_CONFIG* config,
              JobInitFunc job_init,
              JobCallback job_call)
{
    PreforkServer server(*config, job_init, job_call);
    init_set_proc_title(argc, argv);
    server.main();
}

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
