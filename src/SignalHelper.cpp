#include <cstring>
#include <cassert>
#include "Logger.hpp"
#include "Process.hpp"
#include "ProcessPool.hpp"
#include "SignalHelper.hpp"

namespace prefork
{
    int
    SignalHelper::reg_signal(int signo,
                             SignalHandler handler,
                             SignalHandler old_handler /*= NULL*/)
    {
        struct sigaction sigact, oldact;
        memset(&sigact, 0, sizeof(sigact));
        memset(&oldact, 0, sizeof(oldact));            
        sigact.sa_handler = handler;
        sigact.sa_flags = 0;
        sigemptyset(&sigact.sa_mask);
        if (::sigaction(signo, &sigact, &oldact) < 0) { 
            return -1;
        }
        if (old_handler != NULL) {
            old_handler = oldact.sa_handler;
        }
        return 0;
    }
    int
    SignalHelper::reg_sigaction(int signo,
                                SigActionHandler handler,
                                SigActionHandler old_handler/*= NULL*/)
    {
        struct sigaction sigact, oldact;
        memset(&sigact, 0, sizeof(sigact));
        memset(&oldact, 0, sizeof(oldact));
        sigact.sa_sigaction = handler;
        sigact.sa_flags = SA_SIGINFO;
        sigemptyset(&sigact.sa_mask);
        if (::sigaction(signo, &sigact, &oldact) < 0) { 
            return -1;
        }
        if (old_handler != NULL) {
            old_handler = oldact.sa_sigaction;
        }
        return 0;
    }

    void
    SignalHelper::register_signals()
    {
        if (SignalHelper::reg_signal(PROCESS_SIGNAL_TERM,
                                     Process::sighandler) < 0 ||
            SignalHelper::reg_signal(PROCESS_SIGNAL_EXIT,
                                     Process::sighandler) < 0 ||
            SignalHelper::reg_signal(PROCESS_SIGNAL_QUIT,
                                     Process::sighandler) < 0 ||
            SignalHelper::reg_signal(PROCESS_SIGNAL_CHLD,
                                     ProcessPool::sighandler) < 0 ||
            SignalHelper::reg_signal(PROCESS_SIGNAL_MORE_WORKER,
                                     Process::sighandler) < 0 ||
            SignalHelper::reg_signal(PROCESS_SIGNAL_LESS_WORKER,
                                     Process::sighandler) < 0)  {
            assert(false);
            PREFORK_ERR("regiter signal handlers failed!");
        }            
    }
        
    void
    SignalHelper::block_signals()
    {
        sigset_t sigset;
        sigaddset(&sigset, PROCESS_SIGNAL_TERM);
        sigaddset(&sigset, PROCESS_SIGNAL_EXIT);
        sigaddset(&sigset, PROCESS_SIGNAL_QUIT);
        sigaddset(&sigset, PROCESS_SIGNAL_CHLD);
        sigaddset(&sigset, PROCESS_SIGNAL_MORE_WORKER);
        sigaddset(&sigset, PROCESS_SIGNAL_LESS_WORKER);
        if (sigprocmask(SIG_BLOCK, &sigset, NULL) < 0) {
            assert(false);
            PREFORK_ERR("block signal failed");
        }
    }
        
    void
    SignalHelper::allow_signals()
    {
        sigset_t sigset;
        sigemptyset(&sigset);
        if (sigprocmask(SIG_SETMASK, &sigset, NULL) == -1) {
            assert(false);
            PREFORK_ERR("reset procmask failed");
        }            
    }

    void
    SignalHelper::ignore_signals()
    {
        if (SignalHelper::reg_signal(PROCESS_SIGNAL_TERM,
                                     (SignalHandler)SIG_IGN) < 0 ||
            SignalHelper::reg_signal(PROCESS_SIGNAL_EXIT,
                                     (SignalHandler)SIG_IGN) < 0 ||
            SignalHelper::reg_signal(PROCESS_SIGNAL_CHLD,
                                     (SignalHandler)SIG_IGN) < 0 ||
            SignalHelper::reg_signal(PROCESS_SIGNAL_MORE_WORKER,
                                     (SignalHandler)SIG_IGN) < 0 ||
            SignalHelper::reg_signal(PROCESS_SIGNAL_LESS_WORKER,
                                     (SignalHandler)SIG_IGN) < 0) {
            assert(false);
            PREFORK_ERR("ignore signals failed");
        }
    }
        
    void
    SignalHelper::pause_wait_signals()
    {
        sigset_t sigset;
        sigemptyset(&sigset);
        sigsuspend(&sigset);            
    }

    const char*
    SignalHelper::get_signal_name(int signo)
    {
        const char* name = "UNKOWN";
        switch (signo) {
        case PROCESS_SIGNAL_CHLD:
            name = "CHLD";
            break;
        case PROCESS_SIGNAL_TERM:
            name = "TERM";
            break;
        case PROCESS_SIGNAL_EXIT:
            name = "EXIT";
            break;
        case PROCESS_SIGNAL_QUIT:
            name = "QUIT";
            break;
        case PROCESS_SIGNAL_MORE_WORKER:
            name = "MORE_WORKER";
            break;
        case PROCESS_SIGNAL_LESS_WORKER:
            name = "LESS_WORKER";
            break;
        default:
            break;
        }
        return name;
    }        
}; // namespace prefork

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
