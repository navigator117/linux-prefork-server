#ifndef _PREFORK_SERVER_EXPORT_C_H_
#define _PREFORK_SERVER_EXPORT_C_H_

#include "PreforkConfig.h"

#ifdef __cplusplus
extern "C" {
#endif
    typedef int(*JobInitFunc)(void);
    typedef int(*JobCallback)(int sock);
    
    extern void
    prefork_usage(const char* program);    
    extern int
    prefork_parse(int argc,
                  char* argv[],
                  PREFORK_CONFIG* config);
    extern void
    prefork_serve(int argc,
                  char* argv[],
                  const PREFORK_CONFIG* config,
                  JobInitFunc job_init,
                  JobCallback job_call);
#ifdef __cplusplus    
} // extern "C" {
#endif

#endif /* _PREFORK_SERVER_EXPORT_C_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
