#ifndef _PREFORK_SOCKET_H_
#define _PREFORK_SOCKET_H_

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <cassert>

namespace prefork
{
    class Socket
    {
    public:
        explicit Socket(int sock = -1);
        Socket(const Socket& that);
        const Socket& operator=(const Socket& that);
        ~Socket();

        bool is_valid() const;
        int open();
        ssize_t block_send(const unsigned char* const buffer,
                           size_t want_bytes);
        ssize_t block_recv(unsigned char* buffer,
                           size_t want_bytes);
        void close();
        int sock() const;
        void sock(int _sock);

    private:    
        mutable int _sock;
    };

    inline
    Socket::Socket(int sock /* = -1 */) : 
        _sock(sock)
    {
    }

    inline
    Socket::Socket(const Socket& that)
    {
        /* NOTE owner ship changed */
        this->_sock = that.sock();
        that._sock = -1;
    }

    inline
    const Socket& 
    Socket::operator=(const Socket& that)
    {
        /* NOTE owner ship changed */
        if (this != &that) {
            assert(!this->is_valid());
            this->_sock = that.sock();
            that._sock = -1;
        }
        return *this;
    }

    inline
    Socket::~Socket()
    {
        this->close();
    }

    inline
    bool
    Socket::is_valid() const
    {
        return this->_sock != -1;
    }

    inline 
    void
    Socket::close()
    {
        if (this->_sock != -1) {                             
            ::close(this->_sock);
            this->_sock = -1;                
        }
    }

    inline
    int
    Socket::sock() const
    {
        return this->_sock;
    }

    inline
    void
    Socket::sock(int _sock)
    {
        this->_sock = _sock;
    }
}; // namespace prefork

#endif /* _PREFORK_SOCKET_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
