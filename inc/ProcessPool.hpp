#ifndef _PREFORK_PROCESS_POOL_H_
#define _PREFORK_PROCESS_POOL_H_

#include <cstdlib>
#include "Process.hpp"
#include "ProcessGroup.hpp"
#include "SignalHelper.hpp"

namespace prefork
{
    const int MAX_NUM_OF_PROCESS       = 1024;
    const int MAX_NUM_OF_PROCESS_GROUP = MAX_NUM_OF_PROCESS;

    struct SHARED_DATA {            
        volatile int  _spawned;            
        volatile bool _is_busy[MAX_NUM_OF_PROCESS];
    };
        
    class ProcessPool
    {
    public:
        static ProcessPool& get();

        const Process*      get_process(int index);
        const ProcessGroup* get_process_group(int index);            
        const Process*      spawn(ProcessProc proc,
                                  void* data = NULL,
                                  ProcessGroup* process_group = NULL);
        const ProcessGroup* spawn_many(int num_of_process,
                                       ProcessProc proc,
                                       void* data = NULL);
        void respawn_processes();
        void on_process_exit(pid_t child_pid);
        static void sighandler(int signo);

    private:
        ProcessPool();
        virtual ~ProcessPool();

        SHARED_DATA*   _shared;
        Process        _processes[MAX_NUM_OF_PROCESS];
        ProcessGroup   _process_groups[MAX_NUM_OF_PROCESS_GROUP];
    };
}; // namespace prefork

#endif /* _PREFORK_PROCESS_POOL_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
