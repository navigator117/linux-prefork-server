#ifndef _PREFORK_PROCESS_H_
#define _PREFORK_PROCESS_H_

#include "SignalHelper.hpp"

namespace prefork
{
    enum {
        PROCESS_STATUS_UNKNOWN = 0,
        PROCESS_STATUS_STOPPED,
        PROCESS_STATUS_STARTED,
        PROCESS_STATUS_STOPPING,
        PROCESS_STATUS_STARTING
    };

    enum {
        PROCESS_EVENT_NONE        = 0x00000000,
        PROCESS_EVENT_TERM        = 0x00000001,
        PROCESS_EVENT_EXIT        = 0x00000002,
        PROCESS_EVENT_QUIT        = 0x00000004,
        PROCESS_EVENT_CHLD        = 0x00000008,
        PROCESS_EVENT_MORE_WORKER = 0x00000010,
        PROCESS_EVENT_LESS_WORKER = 0x00000020
    };

    struct SHARED_DATA;
    class Process;
    class ProcessGroup;
        
    typedef void (*ProcessProc)(const Process& process, void* data);
                
    class Process
    {
    public:
        Process();
        explicit Process(ProcessProc proc,
                         void* data = NULL,
                         ProcessGroup* process_group = NULL);
        virtual ~Process();

        void  main();
        pid_t fork();

        int   get_index() const;            
        void  set_index(int index);
        SHARED_DATA* get_shared() const;
        void  set_shared(SHARED_DATA* shared);
        pid_t get_pid() const;
        void  set_pid(pid_t pid);
        int   get_status() const;
        void  set_status(int status);
        int   get_events() const;
        void  rst_events() const;
        const char* get_events_desc() const;
        bool  in_child_process() const;
        void  quit(bool wait) const;
        ProcessGroup* get_group() const;
        static void sighandler(int signo);
            
    private:            
        int           _index;
        SHARED_DATA*  _shared;
        pid_t         _pid;
        volatile int  _status;
        ProcessProc   _proc;
        void*         _data;
        ProcessGroup* _group;
            
        static volatile int _events;
    };
}; // namespace prefork

#endif /* _PREFORK_PROCESS_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
