#ifndef _PREFORK_SERVER_H_
#define _PREFORK_SERVER_H_

#include "SockServer.hpp"
#include "ProcessPool.hpp"
#include "PreforkServerExportC.h"

namespace prefork
{
    const int         DEFAULT_WORK_GROUP_INDEX = 0;
    const char* const WATCHER_PROCESS_TITLE    = "watcher";
    const char* const WORKER_PROCESS_TITLE     = "worker";
        
    class PreforkServer : public Process
    {
    public:
        PreforkServer(const PREFORK_CONFIG& config,
                  JobInitFunc job_init = NULL,
                  JobCallback job_call = NULL);
        virtual ~PreforkServer();

    private:
        static void main_proc(const Process& process, PreforkServer* self);
        static void watch_proc(const Process& process, PreforkServer* self);
        static void work_proc(const Process& process, PreforkServer* self);

        void init_network();
        void fini_network();
        void single_main_loop(const Process& process);
        void master_main_loop(const Process& process);
        void init_watcher();
        void fini_watcher();
        void inc_work_groups(int num_of_process_in_group);
        void dec_work_groups(bool wait);


        PREFORK_CONFIG       _config;
        JobInitFunc          _job_init;
        JobCallback          _job_call;
        SockServer           _sock;
        const Process*       _watcher;            
        int                  _free_work_groups_index;
        const ProcessGroup*  _work_groups[MAX_NUM_OF_PROCESS_GROUP];
    };
}; // namespace prefork

#endif /* _PREFORK_SERVER_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
