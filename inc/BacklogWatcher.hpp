#ifndef _PREFORK_BACKLOG_WATCHER_H_
#define _PREFORK_BACKLOG_WATCHER_H_

#include "PreforkConfig.h"

namespace prefork
{
    class BacklogWatcher
    {
    public:
        BacklogWatcher(pid_t parent,
                       const PREFORK_CONFIG& config);
        virtual ~BacklogWatcher();
        void init();
        void fini();            
        void query();
            
    private:
        void tell_parent(unsigned int backlog,
                         unsigned int max_backlog);
            
        pid_t          _parent;
        int            _sock;
        PREFORK_CONFIG _config;
        int            _less_woker_counter;
        int            _more_woker_counter;
    };
}; // namespace prefork

#endif /* _PREFORK_BACKLOG_WATCHER_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
