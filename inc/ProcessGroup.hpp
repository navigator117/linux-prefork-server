#ifndef _PREFORK_PROCESS_GROUP_H_
#define _PREFORK_PROCESS_GROUP_H_

#include "Process.hpp"

namespace prefork
{
    const int MAX_NUM_OF_PROCESS_IN_GROUP = 512;
        
    enum {
        PROCESS_GROUP_STATUS_UNKNOWN = 0,
        PROCESS_GROUP_STATUS_STOPPED,
        PROCESS_GROUP_STATUS_STARTED,
    };
        
    class ProcessGroup
    {
    public:
        ProcessGroup();
        explicit ProcessGroup(int num_of_process);
        virtual ~ProcessGroup();

        int            get_process_count() const;
        int            get_status() const;
        void           set_status(int status);
        const Process* get_process(int index) const;
        void           set_process(int index, const Process* process);
        void           quit(bool wait) const;
            
    private:
        int             _num_of_process;
        volatile int    _status;
        const Process*  _processes[MAX_NUM_OF_PROCESS_IN_GROUP];
    };
}; // namespace prefork

#endif /* _PREFORK_PROCESS_GROUP_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
