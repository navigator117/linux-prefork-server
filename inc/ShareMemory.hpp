#ifndef _PREFORK_SHARE_MEMORY_H_
#define _PREFORK_SHARE_MEMORY_H_

#include <sys/mman.h>
#include <cassert>

namespace prefork
{
    class ShareMemory
    {
    public:
        template <typename T>
        static T* create(void);
        template <typename T>
        static T* remove(T* memory);
    };

    template <typename T>
    T*
    ShareMemory::create(void)
    {
        T* result = NULL;
        result = (T*)mmap(
                          NULL,
                          sizeof(T),
                          PROT_READ | PROT_WRITE,
                          MAP_SHARED | MAP_ANONYMOUS,
                          -1,
                          0);
        assert(result != NULL);
        return result;
    }

    template <typename T>
    T*
    ShareMemory::remove(T* memory)
    {
        int result = munmap(memory, sizeof (T));
        if (result != 0) {
            assert(false);
        }
        return NULL;
    }
}; // namespace prefork

#endif /* _PREFORK_SHARE_MEMORY_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
