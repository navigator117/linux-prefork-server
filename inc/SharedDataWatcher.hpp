#ifndef _PREFORK_SHARED_DATA_WATCHER_H_
#define _PREFORK_SHARED_DATA_WATCHER_H_

#include "PreforkConfig.h"

namespace prefork
{
    struct SHARED_DATA;
    class SharedDataWatcher
    {
    public:
        SharedDataWatcher(pid_t parent,
                          int index,
                          SHARED_DATA* shared,
                          const PREFORK_CONFIG& config);
        virtual ~SharedDataWatcher();

        void init();
        void fini();
        void query();

        pid_t          _parent;
        int            _index;
        SHARED_DATA*   _shared;
        PREFORK_CONFIG _config;
        int            _less_woker_counter;
        int            _more_woker_counter;
    };
}; // namespace prefork

#endif /* _PREFORK_SHARED_DATA_WATCHER_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
