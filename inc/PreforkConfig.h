#ifndef _PREFORK_CONFIG_H_
#define _PREFORK_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif
    const char* const DEFAULT_PROCESS_BASE_NAME       = "prefork";
    const int         DEFAULT_SERVER_PORT             = 8000;
    const int         DEFAULT_BACKLOG_SIZE            = 128;
    const float       DEFAULT_WORKLOAD_CHECK_INTERVAL = 0.5;    // 0.5s
    const float       DEFAULT_INC_WORKER_RATIO        = 0.8;
    const float       DEFAULT_DEC_WORKER_RATIO        = 0.2;
    const int         DEFAULT_INC_WORKER_HITS         = 2;
    const int         DEFAULT_DEC_WORKER_HITS         = 2 * 60; //  60s
    const int         DEFAULT_NUM_OF_WORKER_PROCESS   = 32;
    const int         DEFAULT_INC_OF_WORKER_PROCESS   = 4;
    
    typedef struct {
        char  _process_base_name[256];
        int   _is_daemon;
        int   _is_single_process;
        int   _port;
        int   _backlog;
        float _workload_check_interval;
        float _inc_worker_ratio;
        float _dec_worker_ratio;
        int   _inc_worker_hits;
        int   _dec_worker_hits;
        int   _num_of_workers;
        int   _inc_of_workers;
    } PREFORK_CONFIG;

    extern void
    check_prefork_config(PREFORK_CONFIG* config);
#ifdef __cplusplus    
} // extern "C" {
#endif

#endif /* _PREFORK_CONFIG_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
