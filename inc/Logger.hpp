#ifndef _PREFORK_LOGGER_H_
#define _PREFORK_LOGGER_H_

#include <syslog.h>
#include <cstdio>

#ifdef NDEBUG
#define PREFORK_ERR(format, ...) \
    syslog(LOG_USER | LOG_ERR, "%s,%d,%s:"format, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);
#define PREFORK_WARN(format, ...) \
    syslog(LOG_USER | LOG_WARNING, "%s,%d,%s:"format, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);
#define PREFORK_INFO(format, ...) \
    syslog(LOG_USER | LOG_INFO, "%s,%d,%s:"format, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);
#define PREFORK_DEBUG(format, ...)
#else /* !NDEBUG */
#define PREFORK_ERR(format, ...) \
    fprintf(stderr, "<ERROR> %s,%d,%s:"format"\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);
#define PREFORK_WARN(format, ...) \
    fprintf(stderr, "<WARN>  %s,%d,%s:"format"\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);
#define PREFORK_INFO(format, ...) \
    fprintf(stderr, "<INFO>  %s,%d,%s:"format"\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);
#define PREFORK_DEBUG(format, ...) \
    fprintf(stderr, "<DEBUG> %s,%d,%s:"format"\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);
#endif /* !NDEBUG */

#endif /* _PREFORK_LOGGER_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
