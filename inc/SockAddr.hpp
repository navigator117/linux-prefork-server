#ifndef _PREFORK_SOCKADDR_H_
#define _PREFORK_SOCKADDR_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <cstring>
#include <cassert>
#include <cstdlib>

namespace prefork
{
    class SockAddr {
    public:
        explicit SockAddr(unsigned short port);
        SockAddr(const char* const ip, unsigned short port);
        ~SockAddr();

        const char* const ip() const;
        short port() const;
        const struct sockaddr* const addr(socklen_t& length) const;
        bool is_inaddr_any() const;

    private:
        void conv_addr();

        explicit SockAddr(const SockAddr&);
        const SockAddr& operator=(const SockAddr&);

        char               _ip[128];
        unsigned short     _port;
        struct sockaddr_in _addr;
    };

    inline
    SockAddr::SockAddr(unsigned short port) :
        _port(port)
    {
        assert(port > 0 && port <= 65534 && "invalid port address!");
        memset(&this->_ip, 0, sizeof(this->_ip));
        this->conv_addr();
    }

    inline
    SockAddr::SockAddr(const char* const ip, unsigned short port) :
        _port(port)
    {
        assert(ip != NULL && "invalid ip address!");
        assert(port > 0 && port <= 65534 && "invalid port address!");
        int n = sizeof(this->_ip);
        strncpy(this->_ip, ip, n);
        this->_ip[n-1] = '\0';
        this->conv_addr();
    }

    inline
    SockAddr::~SockAddr()
    {
    }

    inline
    void
    SockAddr::conv_addr()
    {
        memset(&this->_addr, 0, sizeof(this->_addr));
        this->_addr.sin_family = AF_INET;
        this->_addr.sin_port = htons(this->port());
        if (this->is_inaddr_any()) {
            this->_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        }
        else {
            struct hostent* host = NULL;
            if ((host=gethostbyname(this->ip())) != NULL) { 
                memcpy(&this->_addr.sin_addr.s_addr, 
                       host->h_addr, 
                       host->h_length);
            }
        }
    }

    inline
    const char* const
    SockAddr::ip() const
    {
        return this->_ip;
    }

    inline
    short
    SockAddr::port() const
    {
        return this->_port;
    }

    inline
    bool
    SockAddr::is_inaddr_any() const
    {
        return this->_ip[0] == 0;
    }

    inline
    const struct sockaddr* const
    SockAddr::addr(socklen_t& length) const
    {
        length = sizeof(this->_addr);
        return (const struct sockaddr* const)&this->_addr;
    }
}; // namespace prefork

#endif /* _PREFORK_SOCKADDR_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
