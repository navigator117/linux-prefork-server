#ifndef _PREFORK_SIGNAL_HELPER_H_
#define _PREFORK_SIGNAL_HELPER_H_

#include <unistd.h>
#include <signal.h>

namespace prefork
{
    enum {
        PROCESS_SIGNAL_TERM        = SIGTERM,
        PROCESS_SIGNAL_EXIT        = SIGINT,
        PROCESS_SIGNAL_QUIT        = SIGQUIT,
        PROCESS_SIGNAL_CHLD        = SIGCHLD,
        PROCESS_SIGNAL_MORE_WORKER = SIGUSR1,
        PROCESS_SIGNAL_LESS_WORKER = SIGUSR2
    };
        
    typedef void(*SignalHandler)(int);
    typedef void(*SigActionHandler)(int, siginfo_t*, void*);

    class SignalHelper {
    public:
        static int reg_signal(int signo,
                              SignalHandler handler,
                              SignalHandler old_handler = NULL);
        static int reg_sigaction(int signo,
                                 SigActionHandler handler,
                                 SigActionHandler old_handler = NULL);
        static void register_signals();
        static void block_signals();
        static void allow_signals();
        static void ignore_signals();
        static void pause_wait_signals();
        static const char* get_signal_name(int signo);
    };
}; // namespace prefork

#endif /* _PREFORK_SIGNAL_HELPER_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
