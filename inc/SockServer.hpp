#ifndef _PREFORK_SOCK_SERVER_H_
#define _PREFORK_SOCK_SERVER_H_

#include <cerrno>
#include "SockAddr.hpp"
#include "Socket.hpp"

namespace prefork
{
    class SockServer : public Socket
    {
    public:
        SockServer();
        ~SockServer();

        int bind(const SockAddr& addr);
        int listen(int backlog);
        bool accept(Socket& socket);
    };

    inline
    SockServer::SockServer() :
        Socket()
    {
    }

    inline
    SockServer::~SockServer()
    {
    }

    inline
    int
    SockServer::bind(const SockAddr& addr)
    {
        assert(addr.is_inaddr_any());
        socklen_t addr_len;
        const struct sockaddr* const sock_addr = addr.addr(addr_len);
        return ::bind(this->sock(), sock_addr, addr_len);
    }

    inline
    int
    SockServer::listen(int backlog)
    {
        return ::listen(this->sock(), backlog);
    }

    inline
    bool
    SockServer::accept(Socket & socket)
    {
        int accepted_sock = -1;
        while (1) {
            if ((accepted_sock=::accept(this->sock(), 0, 0)) >= 0) {
                break;
            }
            else {
                if (errno == EINTR) {
                    return false;
                }
                else {
                    continue;
                }     
            }
        }
        socket.sock(accepted_sock);
        return true;
    }
}; // namespace prefork

#endif /* _PREFORK_SOCK_SERVER_H_ */
