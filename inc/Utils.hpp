#ifndef _PREFORK_UTILS_H_
#define _PREFORK_UTILS_H_

namespace prefork
{
    int
    run_as_daemon();
    void
    init_set_proc_title(int argc, char** argv);
    bool
    set_proc_title(const char* base, const char* title);
}; // namespace prefork

#endif /* _PREFORK_UTILS_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
