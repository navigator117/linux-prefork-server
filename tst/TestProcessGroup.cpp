#include "TestProcessGroup.hpp"
#include "ProcessPool.hpp"

using namespace prefork;

CPPUNIT_TEST_SUITE_REGISTRATION(TestProcessGroup);

void
TestProcessGroup::setUp()
{
}

void
TestProcessGroup::tearDown()
{
}

static void
processProc1(const Process& process, void* data)
{
    for (;;) {
        SignalHelper::pause_wait_signals();
        if (process.get_events() & PROCESS_EVENT_QUIT) {
            break;
        }
    }
}

void
TestProcessGroup::testQuitWait()
{
    SignalHelper::register_signals();
    SignalHelper::block_signals();
    const ProcessGroup* process_group = 
        ProcessPool::get().spawn_many(4, processProc1);
    SignalHelper::allow_signals();
    CPPUNIT_ASSERT(process_group != NULL);
    process_group->quit(true);
    CPPUNIT_ASSERT(process_group->get_status() ==
                   PROCESS_GROUP_STATUS_STOPPED);
    for (int i=0; i<process_group->get_process_count(); ++i) {
        CPPUNIT_ASSERT(process_group->get_process(i)->get_status() ==
                       PROCESS_STATUS_STOPPED);
    }
}

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
