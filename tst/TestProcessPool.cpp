#include "ProcessPool.hpp"
#include "TestProcessPool.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(TestProcessPool);

using namespace prefork;

void
TestProcessPool::setUp()
{
}

void
TestProcessPool::tearDown()
{
}

static void
processProc1(const Process& process, void* data)
{
    CPPUNIT_ASSERT(process.get_pid() > 0);
    CPPUNIT_ASSERT(process.in_child_process());
    CPPUNIT_ASSERT(process.get_status() == PROCESS_STATUS_STARTED);
    CPPUNIT_ASSERT(data == NULL);
    for (;;) {
        SignalHelper::pause_wait_signals();
        if (process.get_events() & PROCESS_EVENT_QUIT) {
            break;
        }
    }
}

void
TestProcessPool::testSpawn()
{
    SignalHelper::register_signals();
    SignalHelper::block_signals();
    const Process* process = 
        ProcessPool::get().spawn(processProc1);
    SignalHelper::allow_signals();
    CPPUNIT_ASSERT(process != NULL);
    CPPUNIT_ASSERT(process->get_pid() > 0);
    CPPUNIT_ASSERT(!process->in_child_process());
    CPPUNIT_ASSERT(process->get_status() == PROCESS_STATUS_STARTED);
    process->quit(true);
    CPPUNIT_ASSERT(process->get_status() == PROCESS_GROUP_STATUS_STOPPED);
}

const int process_count = 4;
static void
processProc2(const Process& process, void* data)
{
    const ProcessGroup* process_group = process.get_group();
    CPPUNIT_ASSERT(process_group->get_process_count() == process_count);
    CPPUNIT_ASSERT(process_group->get_status() ==
                   PROCESS_GROUP_STATUS_STARTED);
    CPPUNIT_ASSERT(process.get_pid() > 0);
    CPPUNIT_ASSERT(process.in_child_process());
    CPPUNIT_ASSERT(process.get_status() == PROCESS_STATUS_STARTED);
    CPPUNIT_ASSERT(data == NULL);    
    for (;;) {
        SignalHelper::pause_wait_signals();
        if (process.get_events() & PROCESS_EVENT_QUIT) {
            break;
        }
    }
}

void
TestProcessPool::testSpawnMany()
{
    SignalHelper::register_signals();
    SignalHelper::block_signals();
    const ProcessGroup* process_group =
        ProcessPool::get().spawn_many(process_count, processProc2);
    SignalHelper::allow_signals();
    CPPUNIT_ASSERT(process_group->get_process_count() == process_count);
    CPPUNIT_ASSERT(process_group->get_status() ==
                   PROCESS_GROUP_STATUS_STARTED);
    process_group->quit(true);
    CPPUNIT_ASSERT(process_group->get_status() ==
                   PROCESS_GROUP_STATUS_STOPPED);    
}

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
