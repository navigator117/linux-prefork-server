#include <sys/wait.h>
#include "TestProcess.hpp"
#include "ProcessPool.hpp"

using namespace prefork;

CPPUNIT_TEST_SUITE_REGISTRATION(TestProcess);

void
TestProcess::setUp()
{
}

void
TestProcess::tearDown()
{
}

static void
processProc1(const Process& process, void* data)
{
    for (;;) {
        SignalHelper::pause_wait_signals();
        if (process.get_events() & PROCESS_EVENT_QUIT) {
            break;
        }
    }
}

void
TestProcess::testQuitWait()
{
    SignalHelper::register_signals();
    SignalHelper::block_signals();
    const Process* process = 
        ProcessPool::get().spawn(processProc1);
    SignalHelper::allow_signals();    
    CPPUNIT_ASSERT(process != NULL);
    process->quit(true);
    CPPUNIT_ASSERT(process->get_status() == PROCESS_STATUS_STOPPED);
}

static void
processProc2(const Process& process, void* data)
{ 
    for (;;) {
        SignalHelper::pause_wait_signals();
        if (process.get_events() & PROCESS_EVENT_QUIT) {
            break;
        }
    }
}
    
void
TestProcess::testKillRespawn()
{
    SignalHelper::register_signals();
    SignalHelper::block_signals();
    const Process* process = 
        ProcessPool::get().spawn(processProc2);
    CPPUNIT_ASSERT(process != NULL);
    pid_t pid = process->get_pid();
    CPPUNIT_ASSERT(pid > 0);
    CPPUNIT_ASSERT(kill(pid, SIGKILL) >= 0);
    SignalHelper::allow_signals();
    while (sleep(2) > 0);
    SignalHelper::block_signals();
    ProcessPool::get().respawn_processes();
    SignalHelper::allow_signals();
    CPPUNIT_ASSERT(process->get_status() == PROCESS_STATUS_STARTED);
    CPPUNIT_ASSERT(process->get_pid() != pid);
    process->quit(true);
    CPPUNIT_ASSERT(process->get_status() == PROCESS_STATUS_STOPPED);
}

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
