#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "SignalHelper.hpp"
#include "ShareMemory.hpp"
#include "TestShareMemory.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(TestShareMemory);

using namespace prefork;

void
TestShareMemory::setUp()
{
}

void
TestShareMemory::tearDown()
{
}

typedef struct {
    bool status;
} SHARED_DATA;

void
TestShareMemory::test()
{    
    SHARED_DATA* shared =
        ShareMemory::create<SHARED_DATA>();
    CPPUNIT_ASSERT(shared != NULL);
    CPPUNIT_ASSERT(!shared->status);
    if (fork() == 0) {
        // child
        shared->status = true;
        exit(0);
    }
    else {
        // parent
        wait(NULL);
        CPPUNIT_ASSERT(shared->status);
    }
}

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
