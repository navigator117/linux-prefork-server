#ifndef _TEST_PROCESS_POOL_H_
#define _TEST_PROCESS_POOL_H_

#include <cppunit/extensions/HelperMacros.h>

class TestProcessPool : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestProcessPool);
    CPPUNIT_TEST(testSpawn);
    CPPUNIT_TEST(testSpawnMany);
    CPPUNIT_TEST_SUITE_END();
    
public:
    void setUp();
    void tearDown();
    void testSpawn();
    void testSpawnMany();
};

#endif /* _TEST_PROCESS_POOL_H_ */

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
