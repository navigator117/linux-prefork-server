#include <cstring>
#include "TestPreforkServer.hpp"

using namespace prefork;

int
job(int sock)
{
    Socket peer_sock(sock);
    unsigned char buffer[4096];
    while (1) {
        ssize_t want_bytes = sizeof(buffer);
        ssize_t real_bytes = want_bytes;
        if ((real_bytes=peer_sock.block_recv(buffer, want_bytes)) !=
            want_bytes) {
            break;
        }
        if ((real_bytes=peer_sock.block_send(buffer, want_bytes)) !=
            want_bytes) {
            break;
        }
    }
    peer_sock.sock(-1);
    return 0;
}
        
int
main(int argc, char *argv[])
{
    PREFORK_CONFIG config;
    memset(&config, 0, sizeof(config));
    if (prefork_parse(argc, argv, &config) >= 0) {
        prefork_serve(argc, argv, &config, NULL, job);
        return 0;
    }
    else {
        prefork_usage(argv[0]);
        return -1;
    }
}

/**
 * @author XiongWenjie <navigator117@gmail.com>
 * @license This file is part of prefork-library
 * prefork-library is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * prefork-library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with prefork-library.
 * If not, see <http://www.gnu.org/licenses>.
 */
